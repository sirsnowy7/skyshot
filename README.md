# Skyshot
Shooter game made in Python with Pygame. Type `e`, `m`, `h`, or `n` on the CLI to change the difficulty to, respectively, easy, medium, hard, or nightmare. Medium is the default difficulty.
